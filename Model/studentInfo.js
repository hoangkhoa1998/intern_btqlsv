function SinhVien() {
  this.email = "";
  this.maSV = "";
  this.tenSV = "";
  this.gioiTinh = "";
  this.ngayThangNamSinh = "";
  this.diemToan = 0;
  this.diemLy = 0;
  this.diemHoa = 0;
  this.diemAnh = 0;

  this.tinhDiemTrungBinh = function (diemAnh, diemHoa, diemLy, diemToan) {
    return (diemAnh + diemHoa + diemLy + diemToan) / 4;
  };
}

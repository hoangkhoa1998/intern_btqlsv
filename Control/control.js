// BTN ADD STUDENT
const studentArr = [];
document.getElementById("btnThem").onclick = function (e) {
  e.preventDefault();
  const student = new SinhVien();

  const arrInput = document.querySelectorAll(
    ".form-group input, .form-group select"
  );

  for (let input of arrInput) {
    const { id, value } = input;
    student[id] = value;
  }

  studentArr.push(student);

  renderTableStudent(studentArr);
};

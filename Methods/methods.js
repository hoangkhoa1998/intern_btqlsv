function renderTableStudent(studentArr) {
  let html = "";

  for (let index = 0; index < studentArr.length; index++) {
    const student = studentArr[index];
    const method = new SinhVien();

    // convert date format from yyyy-mm-dd to dd-mm-yyyy
    const date = student.ngayThangNamSinh.split("-");
    const newDate = date[2] + "-" + date[1] + "-" + date[0];
    html += `
    <tr>
        <td>${student.maSV}</td>
        <td>${student.tenSV}</td>
        <td>${student.gioiTinh}</td>
        <td>${newDate}</td>
        <td>${student.diemToan}</td>
        <td>${student.diemLy}</td>
        <td>${student.diemHoa}</td>
        <td>${student.diemAnh}</td>
        <td>${method.tinhDiemTrungBinh(
          +student.diemToan,
          +student.diemLy,
          +student.diemHoa,
          +student.diemHoa
        )}</td>
        <td>
            <button onclick = "removeStudent('${
              student.maSV
            }')" class="btn btn-danger mr-3" >Xóa</button>
            <button class="btn btn-primary" >Sửa</button>

        </td>
    </tr>
    `;
  }

  document.getElementById("tableSinhVien").innerHTML = html;
}
